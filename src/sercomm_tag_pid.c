// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * This program generates Sercomm's PID tag.
 *
 * Copyleft (C) 2022 Maximilian Weinmann <x1@disroot.org>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/*
Example:
00000000  30 30 30 31 30 32 30 30  34 33 35 30 34 41 30 30  |0001020043504A00|
00000010  30 30 30 30 30 30 30 30  30 30 30 30 30 30 30 30  |0000000000000000|
00000020  30 30 30 30 30 30 30 30  30 30 30 30 30 30 30 30  |0000000000000000|
00000030  30 30 30 30 30 30 30 30  30 30 30 30 30 30 30 30  |0000000000000000|
00000040  30 30 30 30 30 30 30 30  30 30 30 30 34 31 33 30  |0000000000004130|
00000050  33 30 33 31 30 30 30 30  30 30 30 30 30 30 30 31  |3031000000000001|
00000060  30 30 30 30 32 30 31 35  30 30 30 30 30 30 30 30  |0000201500000000|
00000070
*/

#define SERCOMM_PID_SIZE 0x70

struct Sercomm_PID_t {
	uint8_t HWVER[4];	// example Beeline SmartBox TURBO+
	uint8_t str0x4[4];	// example Beeline SmartBox GIGA
	uint8_t HWID[8];	// AWI->ASCII(415749) example Beeline SmartBox PRO
	uint8_t str0x10[4];	// example WiFire S1500.NBN
	uint8_t str0x14[56];
	uint8_t str0x4c[8]; // A001->ASCII(41303031) example Speedport W 724V Typ C
	uint8_t str0x54[8];
	uint8_t str0x5c[4]; // example Speedport W 724V Typ C
	uint8_t str0x60[4];
	uint8_t SWVER[4];	// example Beeline SmartBox TURBO
	uint8_t str0x68[8];
} PID;

// https://forum.openwrt.org/t/adding-support-for-sercomm-s1500-clones-beeline-smartbox-pro-wifire-s1500-nbn/110065/17?u=maxs0nix

void usage(int status)
{
	FILE *stream = (status != EXIT_SUCCESS) ? stderr : stdout;

	fprintf(stream,
"\n"
"Options:\n"
"  -a <HWVER>      Hardware version\n"
"  -b <str0x4>     example PID Beeline SmartBox GIGA\n"
"  -c <HWID>       Hardware ID\n"
"  -d <str0x10>    example PID WiFire S1500.NBN\n"
"  -e <str0x4c>    A001->ASCII(41303031) example PID Speedport W 724V Typ C\n"
"  -f <str0x5c>    example PID Speedport W 724V Typ C\n"
"  -g <SWVER>      Software version\n"
"  -o <file>       write output to the file <file>\n"
"  -h              show this screen\n"
	);

	exit(status);
}

int main(int argc, char* argv[]) {
	memset(&PID, '0', SERCOMM_PID_SIZE);
	char *pidpath = "sercomm.pid";
	int c;
	while ((c = getopt(argc, argv, "a:b:c:d:e:f:g:ho:")) != -1) {
		switch (c) {
		case 'a':
			memcpy(PID.HWVER, optarg, 4);
			break;
		case 'b':
			memcpy(PID.str0x4, optarg, 4);
			break;
		case 'c': {
			char HWID_ascii[strlen(optarg)*2+1]; //
			for (int i = 0; i<strlen(optarg); i++) {
		        sprintf(HWID_ascii+i*2, "%02x", optarg[i]);
		    }
			memcpy(PID.HWID, HWID_ascii, strlen(HWID_ascii));
			break;			
		}
		case 'd':
			memcpy(PID.str0x10, optarg, 4);
			break;
		case 'e': {
		    char str0x4c_ascii[strlen(optarg)*2+1];
			for (int i = 0; i<strlen(optarg); i++) {
		        sprintf(str0x4c_ascii+i*2, "%02x", optarg[i]);
		    }
			memcpy(PID.str0x4c, str0x4c_ascii, strlen(str0x4c_ascii));
			break;			
		}
		case 'f':
			memcpy(PID.str0x5c, optarg, 4);
			break;
		case 'g':
			memcpy(PID.SWVER, optarg, 4);
			break;
		case 'o':
			pidpath = optarg;
			break;
		case 'h':
			usage(EXIT_SUCCESS);
			break;
		default:
			usage(EXIT_FAILURE);
			break;
		}
	}
	//memcpy(PID.str0x14, "0000", 56);
	//memcpy(PID.str0x54, "00000000", 8);
	//memcpy(PID.str0x60, "0000", 4);
	//memcpy(PID.str0x68, "00000000", 8);
	
	int filepid=open(pidpath, O_WRONLY | O_CREAT, 0644);
	write(filepid, &PID, SERCOMM_PID_SIZE);
	close(filepid);
	return EXIT_SUCCESS;
}
