// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * This program generates a Sercomm kernel header tag 
 * (probably only used for mt7621).
 *
 * Thanks for helping me with this research:
 * Karim Dehouche <karimdplay@gmail.com>
 * Mikhail Zhilkin <csharper2005@gmail.com>
 *
 * Copyright (C) 2022 Maximilian Weinmann <x1@disroot.org>
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "cyg_crc.h"

/*
Example:
00000000: 5365 7200  Ser.
00000004: 2e6f 9601  .o..
00000008: 0020 31e2  . 1.
0000000c: 02ff ffff  ....
00000010: 0001 7001  ..p.
00000014: 2e6e 2600  .n&.
00000018: 639d 177c  c..|
0000001c: 0000 0000  ....
00000020: ffff ffff  ....
00000024: ffff ffff  ....
00000028: 0000 f001  ....
0000002c: 0000 3e01  ..>.
00000030: 7ca3 482c  |.H,
00000034: 0000 0000  ....
00000038: ffff ffff  ....
*/

#define SERCOMM_TAG_SIZE 0x100

struct Sercomm_Tag_Header_Kernel_t {
	uint8_t signSer[4];		// Signature Sercomm
	uint32_t str0x4;		// Start address Kernel + Size Kernel
	uint32_t crcTag;		// CRC32 Tag Header Kernel
	uint8_t str0xC[4];		// Constant (Number of slots?)
	uint32_t addrKern;		// Start address Kernel 
	uint32_t sizeKern;		// Size Kernel
	uint32_t crcKern;		// CRC32 Kernel
	uint32_t str0x1c;		// 0x00000000
	uint32_t str0x20;		// 0xFFFFFFFF
	uint32_t str0x24;		// 0xFFFFFFFF
	uint32_t addrRootfs;	// Start address RootFS 
	uint32_t sizeRootfs;	// Size RootFS
	uint32_t crcRootfs;		// CRC32 RootFS
	uint32_t str0x34;		// 0x00000000
	uint8_t str0x38[200];	// 0xFFFFFFFF
} HDR;

void usage(int status)
{
	FILE *stream = (status != EXIT_SUCCESS) ? stderr : stdout;

	fprintf(stream,
"\n"
"Options:\n"
"  -a <addres>     Start address Kernel (ex. 0x1700100 - Slot1)\n"
"  -b <addres>     Start address RootFS (ex. 0x1f00000 - Slot1)\n"
"  -c <file>       read kernel image from the file <file>\n"
"  -d <file>       read rootfs image from the file <file>\n"
"  -e <file>       write output to the file <file>\n"
"  -f              Hack rootfs - read first 4 byte rootfs\n"
"  -h              show this screen\n"
	);

	exit(status);
}

int main(int argc, char* argv[]) {
	memset(&HDR, 0xFF, SERCOMM_TAG_SIZE);
	HDR.str0xC[0] = 2;	// 0x02FFFFFF
	HDR.str0x1c = 0;
	HDR.str0x34 = 0;
	int fl = 0;
	char *kernaddr, *rootaddr, *kernpath, *rootpath;
	char *tagpath = "sercomm_kernel.tag";
	int c;
	while ((c = getopt(argc, argv, "a:b:c:d:e:fh")) != -1) {
		switch (c) {
		case 'a':
			kernaddr = optarg;
			break;
		case 'b':
			rootaddr = optarg;
			break;
		case 'c':
			kernpath = optarg;
			break;
		case 'd':
			rootpath = optarg;
			break;
		case 'e':
			tagpath = optarg;
			break;
		case 'f':	// Hack rootfs
			fl = 1;
			break;
		case 'h':
			usage(EXIT_SUCCESS);
			break;
		default:
			usage(EXIT_FAILURE);
			break;
		}
	}
	HDR.addrKern = strtol(kernaddr, NULL, 0);
	HDR.addrRootfs = strtol(rootaddr, NULL, 0);
	int fileKern = open(kernpath, O_RDONLY);
	struct stat st;
	fstat(fileKern, &st);
	HDR.sizeKern = st.st_size;
	uint8_t *kernelbuf = (uint8_t*) malloc(HDR.sizeKern);
	read(fileKern, kernelbuf, HDR.sizeKern);
	close(fileKern);
	HDR.crcKern = cyg_ether_crc32(kernelbuf, HDR.sizeKern);
	int fileRootfs = open(rootpath, O_RDONLY);
	if (fl == 0) {
		fstat(fileRootfs, &st);
		HDR.sizeRootfs = st.st_size;
	} else {
		HDR.sizeRootfs = 4;	// Hack read first 4 byte rootfs	
	}
	uint8_t *rootfsbuf = (uint8_t*) malloc(HDR.sizeRootfs);
	if (rootfsbuf==NULL)
		exit (1);
	read(fileRootfs, rootfsbuf, HDR.sizeRootfs);
	HDR.crcRootfs = cyg_ether_crc32(rootfsbuf, HDR.sizeRootfs);
	free(rootfsbuf);
	close(fileRootfs);
	
	HDR.str0x4 = HDR.addrKern + HDR.sizeKern;
/*	At the time of calculation CRC32 Tag Header Kernel, 
	Signature Sercomm (0x0) & CRC32 Tag Header Kernel (0x8) 
	are filled with 0xFFFFFFFF.*/
	HDR.crcTag = cyg_ether_crc32(&HDR, SERCOMM_TAG_SIZE);
	memcpy(HDR.signSer, "Ser\x00", 4);

	int filetag=open(tagpath, O_WRONLY | O_CREAT, 0644);
	write(filetag, &HDR, SERCOMM_TAG_SIZE);
	close(filetag);
	return EXIT_SUCCESS;
}

// gcc sercomm_tag_kernel.c -o sercomm_tag_kernel.out && ./sercomm_tag_kernel.out -a 0x1700100 -b 0x1f00000 -c 2015.4.kern.bin -d 2015.5.rootfs.bin -e kerntag.bin && xxd -c 4 -l 56 kerntag.bin